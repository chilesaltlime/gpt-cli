# for some reason local python file "default_logger.py" can't be found after I added "install_requires" to this setup.py, but it worked before that!
from setuptools import setup, find_packages

# Read the contents of the requirements.txt file
with open("requirements.txt") as f:
    requirements = f.read().splitlines()

setup(
    name="gpt-cli",
    version="0.2.1",
    entry_points={
        "console_scripts": [
            "gpt-cli=dist.gpt_cli:start_repl",  # "gpt-cli" is the command, "gpt_cli" is the module
        ],
    },
    include_package_data=True,
    packages=find_packages(),
    install_requires=requirements,
)
