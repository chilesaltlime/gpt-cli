# README.md

## CLI Chatbot

This repository contains a simple implementation of an AI chatbot using the GPT-4 model of OpenAI made for the terminal.

### Requirements
* python `3.9+`
* OpenAI API key with access to `gpt-4`



### Example

![example](cli-example.png "example")


### Usage

#### Environment Variables 

Place your OpenAI key in the project root in a file named `openai_key.txt`, following the format below:

```txt
OPENAI_API_KEY=<your-api-key-here>
```

The code will read this file and set these values as environment variables for the application's use. These keys are used for authenticating the OpenAI API calls.


#### Installation and usage Running the Bot

You can run the bot with the command below:

```bash
# pip install
pip install -e .

# run from anywhere
gpt-cli
```

Upon running, you'll start a REPL process where you can converse with the Chatbot by entering your text when prompted 'You 💬:'. To stop the bot, simply type `exit()`.

To clear chat history and start over simply type `clear()` into the REPL.

This REPL allows you to add multi-line inputs through hitting 'enter', or pasting text. 
To send input, hit enter twice (be aware that any pasted text will send the text as soon as there is a double-line break.)



#### Logging

By default, chat history is saved to `logs.log` - to disable saving logs to disk, set `save_logs = False` in `logger_default.py`. Log level can be adjusted to `debug` for more detailed info. The `.gitignore` file excludes `logs.log` so conversations are not uploaded to git.

### License

This project is licensed under [MIT license](LICENSE), use it as per your needs.



