from dist.rag_pipeline import WikiChatbot
import os
from pathlib import Path
import sys
import io
import json
import re
from openai import OpenAI


def read_file_to_env():
    # Get the directory of the script
    script_dir = Path(__file__).parent.parent

    # Construct the path to 'openai_key.txt' in the script's directory
    key_file_path = script_dir / "openai_key.txt"

    # Open and read the contents of the file
    with open(key_file_path, "r") as f:
        lines = f.readlines()
        for line in lines:
            key, value = line.split("=")
            os.environ[key] = value.strip()
    OpenAI.organization = os.environ.get("OPENAI_ORG_KEY")
    OpenAI.api_key = os.environ.get("OPENAI_API_KEY")


from dist.markdown_printing import PrettyConsole
from dist.logger_default import custom_logger


class ChatBot:
    """
    A chatbot class that interacts with users and provides responses using the GPT-4 model.

    Attributes:
        logger: The logger object for logging debug information.
        prompt: A list of conversation messages between the user and the chatbot.

    Methods:
        rprint: Prints formatted text using the rich library.
        pretty: Returns a pretty-printed string representation of an object.
        reply_in_chat: Sends a user prompt to the GPT-4 model and returns the generated response.
        save_chat: Saves the chat history to a file.
        send_receive: Processes user input, handles special commands, and generates chatbot responses.
        start_repl: Starts a REPL (Read-Eval-Print Loop) to interact with the chatbot.
    """

    def __init__(self):
        """
        Initializes the ChatBot class.

        Sets up the logger and initializes the prompt list.
        """
        self.logger = custom_logger(__name__, save_logs=True).logger
        self.rag_mode = False
        self.prompt = []
        self.wiki_chat = WikiChatbot(search_new_articles=False)
        read_file_to_env()
        pretty_console = PrettyConsole()
        self.console = pretty_console.console
        self.rprint = pretty_console.rprint
        self.pretty = pretty_console.pretty
        self.system_prompt = [
            {
                "role": "system",
                "content": "you are a helpful, friendly and knowledgeable chatbot that can provide general information about anything. You are a good listener and provide well thought out answers.",
            },
            {
                "role": "assistant",
                "content": f"I am open to any kind of request - can how I serve you?\n\n",
            },
        ]
        self.chat_client = OpenAI(
            base_url="http://localhost:1235/v1", api_key="lm-studio"
        ).chat.completions.create

    def reply_in_chat(self, user: int):
        """
        Sends a user prompt to the LLM model and returns the generated response.

        Args:
            user: The user prompt.

        Returns:
            The generated response from the LLM model.
        """
        self.logger.debug(self.prompt[-1])

        if self.rag_mode:
            rag_results = self.wiki_chat.rag_query(str(self.prompt[-1]))

            yield rag_results

        else:
            completion = self.chat_client(
                model="model-identifier",
                messages=self.prompt,
                temperature=0.5,
            )

            yield completion.choices[0].message.content

    def save_chat(self, filename: str = None):
        """
        Saves the chat history to a file.

        Args:
            filename: The filename to save the chat history to.

        Returns:
            The chat history.
        """
        # use regex to extract the filename from save(filename)

        # save chat history
        script_dir = Path(__file__).parent.parent
        log_file = script_dir / "dist/logs" / filename
        with open(log_file, "w") as file:
            json.dump(self.prompt, file)

        self.rprint(MARKDOWN="\n\t💾 chat history saved 💾\n")
        return self.prompt

    def send_receive(self, user_text: str):
        """
        Processes user input, handles special commands, and generates chatbot responses.

        Args:
            user_text: The user input.

        Returns:
            The updated chat history.
        """

        self.logger.info(self.pretty(user_text))

        self.rprint(MARKDOWN="\t✨🔮✨\n")
        role = "user"
        self.prompt += [{"role": role, "content": f"{'human'}: {user_text}\n"}]
        self.prompt = self.system_prompt + self.prompt

        if user_text.startswith("/wiki"):

            response = self.wiki(user_text)
        elif user_text.startswith("/"):
            response = self.commands(user_text)

        else:
            response = self.reply_in_chat(user_text)

        self.rprint(MARKDOWN=f"\nAI 🧙:\n\n")
        collected_response = ""
        try:
            collected_response = self.rprint(MARKDOWN=None, chunks=response)
        except KeyboardInterrupt:
            self.rprint(MARKDOWN="KeyboardInterrupt detected, stoppping output")

        self.prompt.append(
            {
                "role": "assistant",
                "content": collected_response,
            },
        )
        self.logger.info(f"gpt: {collected_response}")
        return self.prompt

    def wiki(self, user_text):
        if user_text.startswith("/wiki"):
            user_text = user_text.replace("/wiki ", "")
            self.rprint(
                MARKDOWN=f"\n\t📚 wiki chatbot initializing for topic `{user_text}` 📚."
            )
            self.prompt = self.system_prompt + []
            self.rag_mode = True
            rag_output = self.wiki_chat.rag_query(user_text)

            yield rag_output

    def commands(self, user_text):
        if user_text.startswith("/end"):
            self.rprint(MARKDOWN="\n\t📚 wiki chatbot ended 📚.")
            self.rag_mode = False
            return
        if "/exit" in user_text:
            # ask user if they want to save chat history
            self.rprint(
                MARKDOWN=f"\n\t👋 `exit()` detected, would you like to save chat history? (y/n)"
            )
            user_input = input()
            if user_input == "y":
                # ask for filename

                self.rprint(
                    **{
                        "MARKDOWN": "\n\t📂 Please enter a filename to save chat history to:"
                    }
                )
                user_input = input()
                filename = user_input

                self.prompt = self.save_chat(filename)
            self.rprint(MARKDOWN="\n\t👋 Goodbye 👋")

            exit()

        if "load(" in user_text:
            regex = r"load\((.*)\)"
            matches = re.search(regex, user_text)
            filename = matches.group(1)
            script_dir = Path(__file__).parent.parent
            log_file = script_dir / "dist/logs" / filename
            # read json

            with open(log_file, "r") as file:
                self.prompt = json.load(file)

                # load json to string
                prompt = json.dumps(self.prompt)
                prompt.replace("exit()", "")
                prompt.replace("clear()", "")
                prompt.replace("save()", "")
                prompt.replace("load()", "")
                prompt.replace("restart()", "")
                self.prompt = json.loads(prompt)
            # print(self.prompt)
            self.rprint(MARKDOWN="\n\t📂 chat history loaded 📂\n")
            return self.prompt
        if "/restart" in user_text:
            # restart this python program
            self.rprint(MARKDOWN="\n\t🔄 restarting 🔄\n")
            self.rprint(MARKDOWN="\n\t📂 chat history refreshed 📂\n")

            os.execlp("gpt-cli", "gpt-cli")

        if "/save" in user_text:
            regex = r"save\((.*)\)"
            matches = re.search(regex, user_text)
            filename = matches.group(1)
            self.prompt = self.save_chat(filename=filename)
            return self.prompt

        if "/clear" in user_text:
            # clear history
            self.prompt = self.system_prompt + []
            # rprint clear history and a relevant emoji
            self.console.clear()
            self.rprint(MARKDOWN="\n\t🧹 chat history cleared 🧹\n")
            return self.prompt

    def start_repl(self):
        """
        Starts a REPL (Read-Eval-Print Loop) to interact with the chatbot.

        """
        # start REPL to reply in chat

        # repl

        self.logger.info("~~New session started~~")
        self.rprint(welcome_text)
        self.prompt = []

        while True:
            user_text = []
            self.rprint("\n\nYou 💬:\n\t")

            while True:
                # accept multiple lines of user input until user hits enter twice
                line = input()
                if line:
                    user_text.append(line)
                else:
                    break
            user_text = "\n".join(user_text)
            if not user_text.strip():
                continue

            # user_text = "Human: \t" + user_text
            self.prompt = self.send_receive(user_text)


welcome_text = """
# ✨ Welcome traveler, use this program to connect to OpenAI GPT-4 ✨

### Instructions:
- Exit the program by typing `exit()`
- Clear the chat history by typing `clear()`
- Save chat history by typing `save(filename)`
- Load chat history by typing `load(filename)`
- Hit enter twice to send your message


"""

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding="utf-8")


def start_repl():
    chatbot = ChatBot()
    chatbot.start_repl()


if __name__ == "__main__":
    start_repl()
