# https://haystack.deepset.ai/tutorials/27_first_rag_pipeline
from typing import List, Literal
from haystack.utils import ComponentDevice, Secret
from haystack.components.embedders import SentenceTransformersTextEmbedder

from haystack.components.retrievers.in_memory import (
    InMemoryEmbeddingRetriever,
    InMemoryBM25Retriever,
)
from pprint import pprint
from haystack.document_stores.in_memory import InMemoryDocumentStore
from haystack import Document, component

device = ComponentDevice.from_str("cuda:0")
from haystack.components.generators import OpenAIGenerator
from haystack.components.builders import PromptBuilder

from dist.indexing_pipeline import index_docs
from dist.markdown_printing import PrettyConsole

from dist.logger_default import custom_logger

logger = custom_logger(__name__).logger

from dist.utils import Timer

Timer.printer = logger.info
timer_func = Timer.timer_func

embedding_model = "nomic-ai/nomic-embed-text-v1.5"
# embedding_model = "Alibaba-NLP/gte-large-en-v1.5"
pretty_console = PrettyConsole()
console = pretty_console.console
rprint = pretty_console.rprint
pretty = pretty_console.pretty


import wikipedia


def get_wiki_page(title):
    try:
        return wikipedia.page(title=title, auto_suggest=False)
    except Exception as e:
        print(e.args, title)
        return wikipedia.page(title="history", auto_suggest=False)


@component
class ShowDocuments:
    """
    This component shows the documents in the pipeline.
    """

    @component.output_types(documents=List[Document])
    def run(self, documents: List[Document]):
        return {"documents": documents}


import pickle


class WikiChatbot:
    # search method can be "bm25" or "embedding" or "hybrid"
    def __init__(
        self,
        num_docs: int = 10,
        search_method: Literal["bm25", "embedding", "hybrid"] = "hybrid",
        pickled_docs: bool = True,
        search_new_articles: bool = True,
    ):
        self.search_new_articles = search_new_articles
        self.pickled_docs = pickled_docs
        self.num_docs = num_docs
        self.titles_all = set()
        self.generator = OpenAIGenerator(
            api_base_url="http://localhost:1235/v1",
            api_key=Secret.from_token("lm-studio"),
        )
        self.document_store = self.init_docs()
        self.search_method = search_method
        self.basic_rag_pipeline = self.pipeline()

    def init_docs(self):
        file_name = "docs.pkl"
        import os

        if not (self.pickled_docs and os.path.exists(file_name)):
            document_store = InMemoryDocumentStore(
                embedding_similarity_function="cosine"
            )
        else:

            with open(file_name, "rb") as f:
                docs = pickle.load(f)

                document_store = InMemoryDocumentStore(
                    embedding_similarity_function="cosine"
                )
                document_store.write_documents(docs)
            self.titles_all = set(
                [doc.meta["title"] for doc in document_store.storage.values()]
            )
            rprint(f"Loaded {len(self.titles_all)} articles from {file_name}")
        return document_store

    @timer_func
    def query_search(self, user_query: str):
        if self.search_new_articles:
            self.potential_topics = self.get_search_keywords(user_query)
            rprint(f"Potential topics: {self.potential_topics}")

        self.embed_wiki_docs()

    @timer_func
    def get_wiki_text(self, titles: list):

        wiki_page_names = set()
        wiki_pages = []

        from concurrent.futures import ThreadPoolExecutor

        with ThreadPoolExecutor() as executor:  # parallelize the search
            futures = []
            for title in titles:
                future = executor.submit(wikipedia.search, title)
                futures.append(future)

            for future in futures:
                wiki_topic_search = future.result()[:3]
                wiki_page_names.update(wiki_topic_search)

            wiki_page_futures = []

            if self.num_docs is not None:
                wiki_page_names = list(wiki_page_names)[: self.num_docs]

            wiki_page_names = set(wiki_page_names) - self.titles_all
            rprint(f"Downloading {len(wiki_page_names)} wiki pages...")
            for title in wiki_page_names:
                if "disambiguation" in title:
                    continue

                future = executor.submit(get_wiki_page, title=title)
                wiki_page_futures.append(future)

            for future in wiki_page_futures:
                try:
                    page = future.result()
                    wiki_pages.append(page)
                except Exception as e:
                    rprint(e.args, title)
                    continue
            from collections import Counter
            from itertools import chain

            all_links = [page.links for page in wiki_pages]

            all_links_flat = [
                c for c in chain.from_iterable(all_links) if "identifier" not in c
            ]
            self.titles_all = self.titles_all | wiki_page_names
            common = Counter(all_links_flat).most_common(3)
            rprint(f"getting most common links: {common}")
            common_link_pages = [
                get_wiki_page(common_link[0])
                for common_link in common
                if common_link[0].title not in self.titles_all
            ]

            wiki_pages += common_link_pages
            common_link_titles = set(page.title for page in common_link_pages)
            print(f"common links: {common_link_titles}")
            print(f"common links pages: {common_link_pages}")
            # print(f"common link titles: {common_link_titles}")

            self.titles_all = self.titles_all | common_link_titles
            # print(f"wiki_pages: {wiki_pages}")
            # print(f"titles_all: {self.titles_all}")

            return wiki_pages

    @timer_func
    def embed_wiki_docs(self):
        if self.search_new_articles:
            wiki_raw = self.get_wiki_text(self.potential_topics)

            self.topics = set([page.title for page in wiki_raw])
            rprint(
                f"Downloaded the following articles to chat about: {self.topics}. Ask me anything about this topic and I'll read from these pages. To exit the wiki chat, type /end."
            )

            rprint("Writing documents...")
            if wiki_raw:

                index_docs(wiki_raw, self.document_store, self.search_method)
            if self.pickled_docs:
                import pickle

                file_name = "docs.pkl"
                with open(file_name, "wb") as f:
                    doc_values = self.document_store.storage.values()
                    pickle.dump(list(doc_values), f)

    @timer_func
    def get_search_keywords(self, user_query: str):
        rprint("Generating wiki search keywords...")
        topics = self.generator.run(
            f"""Give me 4 probable wikipedia page titles relevant to the following natural language query: `{user_query}`. 
            Purpose: We are searching wikipedia for multiple documents to form a knowledge base related to the query.
            
            Format: Pipe operator `|` separated string of simple wikipedia search topics. Please provide no additional text in the response.
            Examples: 
                - "Why are there maggots in my cherries?" -> "maggots|cherries|agricultural pests|fruit trees" 
                - "How safe is hypochlorous acid for fogging?" -> "hypochlorous acid|fogging|disinfectant|santizers" 
                - "Why am I so allergic right now" -> "environmental allergens|pollen|allergic rhinitis|allergy"
                - "How is the 2024 election going to be different?" -> "2024|2024 United States presidential election|Joe Biden|Donald Trump"
                - "What were the biggest news stories of 1986?" -> "1986|1986 in the United States|1986 in politics|1986 in the world"
                - "What is 'Safe mode' in computing?" -> "Safe mode|Popular Devices|Bootloader|Computer Booting"
            """
        )
        return [user_query] + [t.strip() for t in topics["replies"][0].split("|")]

    @timer_func
    def pipeline(self):

        template = """
        Given the following information from Wikipedia, answer the User's question.
        
        Critical: 
            - As the wiki articles have been updated more recently than your training material, They may seem to occur in the future. This is why the information is provided, please use it!!
            - Provided snippets may have limited relevance, curate them accordingly and exclude or qualify articles that are tagential, unrelated, or not useful.
            - Please cite the exact url provided with each snippet, in markdown link format.

        Context:
            {% for doc in documents %}
                snippet: {{ doc.content }}
                url: {{ doc.meta['url'] }}
            {% endfor %}

        Question: {{question}}
        Answer:
                
               
        
        """
        from haystack.components.joiners import DocumentJoiner

        from haystack.components.rankers import TransformersSimilarityRanker

        from haystack import Pipeline

        document_joiner = DocumentJoiner(top_k=10, join_mode="reciprocal_rank_fusion")
        prompt_builder = PromptBuilder(
            template=template,
        )

        pipeline_generator = OpenAIGenerator(
            api_base_url="http://localhost:1235/v1",
            api_key=Secret.from_token("lm-studio"),
        )

        ranker = TransformersSimilarityRanker(
            model="BAAI/bge-reranker-base",
            device=device,
            meta_fields_to_embed=["title", "summary"],
            top_k=5,
            score_threshold=0.5,
        )
        basic_rag_pipeline = Pipeline()
        basic_rag_pipeline.add_component("show_documents", instance=ShowDocuments())
        basic_rag_pipeline.add_component("prompt_builder", prompt_builder)
        basic_rag_pipeline.add_component("llm", pipeline_generator)
        basic_rag_pipeline.add_component("ranker", ranker)

        if self.search_method == "bm25":
            bm25_retriever = InMemoryBM25Retriever(self.document_store)
            basic_rag_pipeline.add_component("bm25_retriever", bm25_retriever)
            basic_rag_pipeline.connect("bm25_retriever", "ranker")

        elif self.search_method == "embedding":
            text_embedder = SentenceTransformersTextEmbedder(
                model=embedding_model,
                trust_remote_code=True,
                device=device,
            )
            embedding_retriever = InMemoryEmbeddingRetriever(self.document_store)
            basic_rag_pipeline.add_component("text_embedder", text_embedder)
            basic_rag_pipeline.add_component("embedding_retriever", embedding_retriever)
            basic_rag_pipeline.connect(
                "text_embedder.embedding", "embedding_retriever.query_embedding"
            )
            basic_rag_pipeline.connect("embedding_retriever", "ranker")

        elif self.search_method == "hybrid":

            text_embedder = SentenceTransformersTextEmbedder(
                model=embedding_model,
                trust_remote_code=True,
                device=device,
            )
            bm25_retriever = InMemoryBM25Retriever(self.document_store)
            embedding_retriever = InMemoryEmbeddingRetriever(self.document_store)
            basic_rag_pipeline.add_component("bm25_retriever", bm25_retriever)
            basic_rag_pipeline.add_component("text_embedder", text_embedder)
            basic_rag_pipeline.add_component("embedding_retriever", embedding_retriever)
            basic_rag_pipeline.add_component("document_joiner", document_joiner)

            basic_rag_pipeline.connect(
                "text_embedder.embedding", "embedding_retriever.query_embedding"
            )

            basic_rag_pipeline.connect("bm25_retriever", "document_joiner")
            basic_rag_pipeline.connect("embedding_retriever", "document_joiner")
            basic_rag_pipeline.connect("document_joiner", "ranker")

        basic_rag_pipeline.connect("ranker", "prompt_builder.documents")
        basic_rag_pipeline.connect("ranker", "show_documents.documents")

        basic_rag_pipeline.connect("prompt_builder", "llm")
        # basic_rag_pipeline.draw("pipeline.png")
        return basic_rag_pipeline

    @timer_func
    def rag_query(self, question):

        self.query_search(question)
        run_args = {"prompt_builder": {"question": question}}
        if self.search_method in ("hybrid", "bm25"):
            run_args["bm25_retriever"] = {"query": question}
        if self.search_method in ("hybrid", "embedding"):
            run_args["text_embedder"] = {"text": question}

        run_args["ranker"] = {"query": question}

        response = self.basic_rag_pipeline.run(run_args)

        scores = {
            document.meta["title"]: document.score
            for document in response["show_documents"]["documents"]
        }
        logger.debug("documents")
        from pprint import pprint

        for doc in response["show_documents"]["documents"]:
            pprint(doc)
            # logger.warning([doc.meta["title"], doc.score, doc.content, doc.meta["url"]])
        pprint(
            f"Ranker score: {dict(sorted(scores.items(),key=lambda kv:kv[1]),reverse=True)}"
        )
        return response["llm"]["replies"][0]


if __name__ == "__main__":

    # wiki_rag = WikiChatbot(search_method="bm25")
    wiki_rag = WikiChatbot(search_method="embedding")
    # wiki_rag = WikiChatbot(search_method="hybrid")
    wiki_rag.search_new_articles = True
    wiki_rag.pickled_docs = True
    response = wiki_rag.rag_query("Who are some left handed musicians?")

    print(response)

    # for example in examples:
    #     wiki_rag.rag_query(example)


# from dist.rag_pipeline import demo
# r=demo()
# r
