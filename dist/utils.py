def extract_pdf(file_path):
    import fitz

    pdf_document = fitz.open(file_path)
    pdf_text = ""
    for page_num in range(pdf_document.page_count):
        page = pdf_document.load_page(page_num)
        pdf_text += page.get_text()

    with open("pdf_text.txt", "w") as f:
        f.write(str(pdf_text.encode("utf-8")))
    return [str(pdf_text.encode("utf-8"))]


# raw_docs = extract_pdf("../Porta_414mkII_manual.pdf")
from time import time


class Timer:

    printer = print

    @staticmethod
    def timer_func(func):
        # This function shows the execution time of
        # the function object passed
        def wrap_func(*args, **kwargs):
            Timer.printer(f"\t- Start function {func.__name__!r}")
            t1 = time()
            result = func(*args, **kwargs)
            t2 = time()
            Timer.printer(
                f"\t- End function {func.__name__!r} executed in {(t2-t1):.4f}s"
            )
            return result

        return wrap_func
