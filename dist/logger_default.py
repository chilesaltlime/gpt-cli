import logging

import pytz
from datetime import datetime
from pathlib import Path


class custom_logger:
    def __init__(self, name, save_logs=False) -> None:
        self.name = name
        # create logger
        self.logger = logging.getLogger(self.name)
        self.logger.setLevel(logging.WARNING)

        # create console handler and set level to info
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)

        # create formatter
        formatter = logging.Formatter(
            "%(asctime)s PT - %(levelname)s - %(name)s - %(funcName)s - %(lineno)d - %(message)s",
            "%Y-%m-%d %H:%M:%S",
        )

        # set timezone to pacific
        pacific_tz = pytz.timezone("US/Pacific")
        formatter.converter = lambda *args: datetime.now(pacific_tz).timetuple()

        # add formatter to ch
        ch.setFormatter(formatter)

        # add ch to logger
        self.logger.addHandler(ch)
        script_dir = Path(__file__).parent.parent / "dist/logs"

        if save_logs:
            # create new log file with timestamp when script starts
            raw_log_path = (
                script_dir / f"{datetime.now().strftime('%Y-%m-%d-%H-%M-%S')}.log"
            )
            fh_raw = logging.FileHandler(raw_log_path, encoding="utf-8")
            fh_raw.setLevel(logging.INFO)
            formatter_raw = logging.Formatter("%(message)s")
            fh_raw.setFormatter(formatter_raw)
            self.logger.addHandler(fh_raw)

            # add a new stream handler to check for 'clear()'
            sh = logging.StreamHandler()
            sh.setLevel(logging.INFO)
            sh.setFormatter(formatter)

            def check_for_clear(record):
                if "clear()" in record.getMessage():
                    new_raw_log_path = (
                        script_dir
                        / f"{datetime.now().strftime('%Y-%m-%d-%H-%M-%S')}.log"
                    )
                    new_fh_raw = logging.FileHandler(new_raw_log_path, encoding="utf-8")
                    new_fh_raw.setLevel(logging.INFO)
                    new_formatter_raw = logging.Formatter("%(message)s")
                    new_fh_raw.setFormatter(new_formatter_raw)
                    self.logger.addHandler(new_fh_raw)

            sh.addFilter(check_for_clear)
            self.logger.addHandler(sh)


from time import time


# if __name__ == "__main__":
#     logger = custom_logger(__name__, save_logs=False)
