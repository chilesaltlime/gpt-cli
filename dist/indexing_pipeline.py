embedding_model = "nomic-ai/nomic-embed-text-v1.5"  #
# embedding_model = "Alibaba-NLP/gte-large-en-v1.5"
from haystack import Pipeline
from haystack.components.preprocessors import DocumentCleaner, DocumentSplitter
from haystack.components.embedders import SentenceTransformersDocumentEmbedder
from haystack.components.writers import DocumentWriter
from haystack.document_stores.types import DuplicatePolicy
from haystack.utils import ComponentDevice
from haystack import Document
from dist.logger_default import custom_logger

logger = custom_logger(__name__).logger
from haystack.components.caching import CacheChecker
from haystack.utils import ComponentDevice

from dist.utils import Timer

Timer.printer = logger.info
timer_func = Timer.timer_func
from typing import List, Literal

device = ComponentDevice.from_str("cuda:0")


@timer_func
def create_indexing_pipeline(document_store, search_method):

    if search_method != "bm25":
        document_embedder = SentenceTransformersDocumentEmbedder(
            model=embedding_model,
            trust_remote_code=True,
            device=device,
            meta_fields_to_embed=["title"],  # "categories"],
            batch_size=4,
        )
        document_embedder.warm_up()

    document_cleaner = DocumentCleaner()

    class LineBreakSplitter(DocumentSplitter):
        def __init__(self, split_by: str, split_length: int, split_overlap: int):
            super().__init__(
                split_by=split_by,
                split_length=split_length,
                split_overlap=split_overlap,
            )

        def _split_into_units(
            self, text: str, split_by: Literal["word", "sentence", "passage", "page"]
        ) -> List[str]:
            if split_by == "page":
                split_at = "\f"
            elif split_by == "passage":
                split_at = "\n"
            elif split_by == "sentence":
                split_at = "."
            elif split_by == "word":
                split_at = " "
            else:
                raise NotImplementedError(
                    "DocumentSplitter only supports 'word', 'sentence', 'page' or 'passage' split_by options."
                )
            units = text.split(split_at)

            # Add the delimiter back to all units except the last one
            for i in range(len(units) - 1):
                units[i] += split_at
            return units

    document_splitter = LineBreakSplitter(
        split_by="passage", split_length=2, split_overlap=1
    )

    document_splitter_2 = LineBreakSplitter(
        split_by="sentence", split_length=5, split_overlap=2
    )

    document_writer = DocumentWriter(
        document_store=document_store, policy=DuplicatePolicy.OVERWRITE
    )

    indexing_pipeline = Pipeline()
    indexing_pipeline.add_component(
        instance=CacheChecker(document_store, cache_field="title"),
        name="cache_checker",
    )
    indexing_pipeline.add_component("cleaner", document_cleaner)
    indexing_pipeline.add_component("splitter", document_splitter)
    indexing_pipeline.add_component("splitter_2", document_splitter_2)
    indexing_pipeline.add_component("writer", document_writer)

    indexing_pipeline.connect("cache_checker.misses", "cleaner.documents")
    indexing_pipeline.connect("cleaner", "splitter")
    indexing_pipeline.connect("splitter", "splitter_2")

    if search_method != "bm25":
        logger.info("embedding documents")
        indexing_pipeline.add_component("embedder", document_embedder)
        indexing_pipeline.connect("splitter_2", "embedder")
        indexing_pipeline.connect("embedder", "writer")
    else:
        indexing_pipeline.connect("splitter_2", "writer")
    # indexing_pipeline.draw("index_pipeline.png")

    return indexing_pipeline


@timer_func
def index_docs(raw_docs, document_store, search_method):
    # document_store = InMemoryDocumentStore(embedding_similarity_function="cosine")

    documents = [
        Document(
            content=page.content,
            meta={
                "title": page.title,
                "links": len(page.links),
                "summary": page.summary,
                "url": page.url,
            },
        )
        for page in raw_docs
    ]

    indexing_pipeline = create_indexing_pipeline(document_store, search_method)

    indexing_pipeline.run({"cache_checker": {"items": documents}})

    return document_store


# if __name__ == "__main__":
#     some_bands = """The Beatles,The Cure""".split(",")

#     raw_docs = []
#     import wikipedia

#     for title in some_bands:
#         page = wikipedia.page(title=title, auto_suggest=False)
#         doc = Document(
#             content=page.content,
#             meta={"title": page.title, "summary": page.summary, "url": page.url},
#         )
#         raw_docs.append(doc)

#     indexing_pipeline = index_docs(raw_docs)
#     logger.info(indexing_pipeline)
