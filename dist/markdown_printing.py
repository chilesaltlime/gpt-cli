from rich.console import Console
from rich.markdown import Markdown
from rich import pretty
from rich.live import Live
from dist.logger_default import custom_logger

import pprint


class PrettyConsole:
    def __init__(self):
        """
        Initializes the ChatBot class.

        Sets up the logger and initializes the prompt list.
        """
        self.logger = custom_logger(__name__, save_logs=True).logger

        pretty.install()
        self.console = Console()

    def rprint(self, MARKDOWN: str = None, chunks=None):
        """
        Prints formatted text using the rich librasry.

        Args:
            MARKDOWN: The markdown text to be printed.
        """
        if MARKDOWN:
            md = Markdown(MARKDOWN)
            self.console.print(md, end=" ")
            return

        class Container:
            def __init__(self) -> None:
                self.MARKDOWN = ""

            def next(self, ch):
                self.MARKDOWN += ch
                return Markdown(self.MARKDOWN)

        container = Container()

        with Live(
            container.next(next(chunks)),
            refresh_per_second=10,
            vertical_overflow="crop",
        ) as live:
            for ch in chunks:
                live.update(container.next(ch), refresh=True)
        return container.MARKDOWN

    def pretty(self, s: str):
        """
        Returns a pretty-printed string representation of an object.

        Args:
            s: The object to be pretty-printed.

        Returns:
            A pretty-printed string representation of the object.
        """
        return pprint.pformat(s)
